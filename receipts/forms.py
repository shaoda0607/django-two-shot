from django.forms import DateInput, ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ThisIsMyCalendarInput(DateInput):
    input_type = "datetime-local"


class ReceiptForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super(ReceiptForm, self).__init__(*args, **kwargs)
        self.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.user
        )
        self.fields["account"].queryset = Account.objects.filter(
            owner=self.user
        )

    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]
        widgets = {
            "date": ThisIsMyCalendarInput(format="%Y-%m-%dT%H:%M"),
        }


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
