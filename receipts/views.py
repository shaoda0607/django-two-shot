from django.shortcuts import redirect, render
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def receipt_list(request):
    user = request.user
    receipt_list = Receipt.objects.filter(purchaser=user)
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/receipts_list.html", context)


@login_required
def receipt_create(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST, user=request.user)
        if form.is_valid():
            current_user = request.user
            saved_form = form.save(False)
            saved_form.purchaser = current_user
            saved_form.save()
            # print(redirect("home"))
            return redirect("home")

    form = ReceiptForm(user=request.user) # hererererererererer
    context = {"form": form}
    return render(request, "receipts/receipt_create.html", context)


@login_required
def category_list(request):
    current_user = request.user
    category_list = ExpenseCategory.objects.filter(owner=current_user)
    context = {"category_list": category_list}
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    current_user = request.user
    category_list = Account.objects.filter(owner=current_user)
    context = {"account_list": category_list}
    return render(request, "receipts/account_list.html", context)


@login_required
def category_create(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            current_user = request.user
            saved_form = form.save(False)
            saved_form.owner = current_user
            saved_form.save()
            return redirect("category_list")
    form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/category_create.html", context)


@login_required
def account_create(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            current_user = request.user
            saved_form = form.save(False)
            saved_form.owner = current_user
            saved_form.save()
            return redirect("account_list")
    form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/account_create.html", context)
